# CHANGELOG



## v0.1.1 (2024-04-07)

### Fix

* fix(deploy): add deploy package ([`fe82625`](https://gitlab.com/estanislao.cello/my-package/-/commit/fe82625e680613a07097970884a843814c8913c3))


## v0.1.0 (2024-04-07)

### Feature

* feat(test): add new test module ([`69923c2`](https://gitlab.com/estanislao.cello/my-package/-/commit/69923c21cb126a7df3fefa3c531e9e3c7c7060b1))


## v0.0.0 (2024-04-07)

### Unknown

* Update .gitlab-ci.yml file ([`5807233`](https://gitlab.com/estanislao.cello/my-package/-/commit/5807233fd85790f1e8e5bfb571ff99147d947b79))

* add semantic_release config ([`2630797`](https://gitlab.com/estanislao.cello/my-package/-/commit/2630797d9fddb32e4450ae58d492d9f712d2dc97))

* Add new file ([`5768c31`](https://gitlab.com/estanislao.cello/my-package/-/commit/5768c31f4d11f96858229d58da051402dfb2d125))

* Update .gitlab-ci.yml file ([`d4bdb84`](https://gitlab.com/estanislao.cello/my-package/-/commit/d4bdb84767c18f402241b006d464a53ecf002561))

* Add new file ([`ef35fc3`](https://gitlab.com/estanislao.cello/my-package/-/commit/ef35fc31d14bb08c0bc3ab9050e7c0062617e56f))

* Initial commit ([`8141cf2`](https://gitlab.com/estanislao.cello/my-package/-/commit/8141cf2b9abde9762d82b9da558e29032ba31267))
